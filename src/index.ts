
import { readdirSync, readFileSync, writeFileSync } from "fs";
import { AttributeTrack, FeatureType, RuntasticEvent } from "./RuntasticEvent";


console.log("Convert start")

let influxData = ""
let files = readdirSync("data/runtastic")


files.forEach(file => {
    const fileData = readFileSync("data/runtastic/" + file, { encoding: "utf-8" })
    const event: RuntasticEvent = Object.assign(new RuntasticEvent(), JSON.parse(fileData.toString()))


    influxData += `Runtastic,activity=${event.getSportTypeText().replace(" ", "")} duration=${event.getDurationInHours()},calories=${event.calories},distance=${event.getDistance()} ${event.start_time}\n`
})

writeFileSync("data/influxdb/data.csv", influxData)

console.log("Convert finished")