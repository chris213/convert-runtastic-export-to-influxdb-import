export const MILLISECOUNDS_TO_HOUR = 3600000


export class RuntasticEvent {
    version: number = 0
    duration: number = 0
    pause: number = 0
    calories: number = 0
    subjective_feeling: string = ""
    dehydration_volume: number = 0
    plausible: boolean = true
    start_time_timezone_offset: number = 0
    end_time_timezone_offset: number = 0
    tracking_method: string = ""
    start_time: number = 0
    end_time: number = 0
    user_perceived_start_time: number = 0
    user_perceived_end_time: number = 0
    created_at: number = 0
    updated_at: number = 0
    features: EventFeature[] = []
    id: string = ""
    user_id: string = ""
    sport_type_id: string = ""
    creation_application_id: string = ""

    private sportTypes = new Map<string, string>();



    constructor() {
        this.sportTypes.set("1", "Running");
        this.sportTypes.set("2", "Nordic Walking");
        this.sportTypes.set("3", "Cycling");
        this.sportTypes.set("4", "Mountain Biking");
        this.sportTypes.set("5", "Other");
        this.sportTypes.set("6", "Inline Skating");
        this.sportTypes.set("7", "Hiking");
        this.sportTypes.set("8", "Cross-country skiing");
        this.sportTypes.set("9", "Skiing");

        this.sportTypes.set("10", "Snowboarding");
        this.sportTypes.set("11", "Motorbike");
        this.sportTypes.set("12", "xx");
        this.sportTypes.set("13", "Snowshoeing");
        this.sportTypes.set("14", "Treadmill");
        this.sportTypes.set("15", "Ergometer");
        this.sportTypes.set("16", "Elliptical");
        this.sportTypes.set("17", "Rowing");
        this.sportTypes.set("18", "Swimming");
        this.sportTypes.set("19", "Walking");
        this.sportTypes.set("20", "Riding");

        this.sportTypes.set("21", "Golfing");
        this.sportTypes.set("22", "Race Cycling");
        this.sportTypes.set("23", "Tennis");
        this.sportTypes.set("24", "Badminton");
        this.sportTypes.set("25", "Squash");
        this.sportTypes.set("26", "Yoga");
        this.sportTypes.set("27", "Aerobics");
        this.sportTypes.set("28", "Martial Arts");
        this.sportTypes.set("29", "Sailing");

        this.sportTypes.set("30", "Windsurfing");
        this.sportTypes.set("31", "Pilates");
        this.sportTypes.set("32", "Rock Climbing");
        this.sportTypes.set("33", "Frisbee");
        this.sportTypes.set("34", "Strength Training");
        this.sportTypes.set("35", "Volleyball");
        this.sportTypes.set("36", "Handbike");
        this.sportTypes.set("37", "Cross Skating");
        this.sportTypes.set("38", "Soccer");

        this.sportTypes.set("42", "Surfing");
        this.sportTypes.set("43", "Kitesurfing");
        this.sportTypes.set("44", "Kayaking");
        this.sportTypes.set("45", "Basketball");
        this.sportTypes.set("46", "Spinning");
        this.sportTypes.set("47", "Paragliding");
        this.sportTypes.set("48", "Wakeboarding");

        this.sportTypes.set("50", "Diving");
        this.sportTypes.set("51", "Table Tennis");
        this.sportTypes.set("52", "Handball");
        this.sportTypes.set("53", "Back-country skiing");
        this.sportTypes.set("54", "Ice Skating");
        this.sportTypes.set("55", "Sledding");
        this.sportTypes.set("58", "Curling");

        this.sportTypes.set("60", "Biathlon");
        this.sportTypes.set("61", "Kite Skiing");
        this.sportTypes.set("62", "Speed Skiing");
        this.sportTypes.set("63", "PushUps");
        this.sportTypes.set("64", "SitUps");
        this.sportTypes.set("65", "PullUps");
        this.sportTypes.set("66", "Squats");
        this.sportTypes.set("67", "American Football");
        this.sportTypes.set("68", "Baseball");
        this.sportTypes.set("69", "Crossfit");

        this.sportTypes.set("70", "Dancing");
        this.sportTypes.set("71", "Ice Hockey");
        this.sportTypes.set("72", "Skateboarding");
        this.sportTypes.set("73", "Zumba");
        this.sportTypes.set("74", "Gymnastics");
        this.sportTypes.set("75", "Rugby");
        this.sportTypes.set("76", "Standup Paddling");
        this.sportTypes.set("77", "Sixpack");
        this.sportTypes.set("78", "Butt Training");

        this.sportTypes.set("80", "Leg Training");
        this.sportTypes.set("81", "Results Workout");
        this.sportTypes.set("82", "Trail Running");
        this.sportTypes.set("84", "Plogging");
        this.sportTypes.set("85", "Wheelchair");
        this.sportTypes.set("86", "E Biking");
        this.sportTypes.set("87", "Scootering");
        this.sportTypes.set("88", "Rowing Machine");
        this.sportTypes.set("89", "Stair Climbing");

        this.sportTypes.set("90", "Jumping Rope");
        this.sportTypes.set("91", "Trampoline");
        this.sportTypes.set("92", "Bodyweight Training");
        this.sportTypes.set("93", "Tabata");
        this.sportTypes.set("94", "Callisthenics");
        this.sportTypes.set("95", "Suspension Training");
        this.sportTypes.set("96", "Powerlifting");
        this.sportTypes.set("97", "Olympic Weightlifting");
        this.sportTypes.set("98", "Stretching");
        this.sportTypes.set("99", "Mediation");

        this.sportTypes.set("100", "Bouldering");
        this.sportTypes.set("101", "Via Ferrata");
        this.sportTypes.set("102", "Pade");
        this.sportTypes.set("103", "Pole Dancing");
        this.sportTypes.set("104", "Boxing");
        this.sportTypes.set("105", "Cricket");
        this.sportTypes.set("106", "Field Hockey");
        this.sportTypes.set("107", "Track Field");
        this.sportTypes.set("108", "Fencing");
        this.sportTypes.set("109", "Skydiving");

        this.sportTypes.set("111", "Cheerleading");
        this.sportTypes.set("112", "E-Sports");
        this.sportTypes.set("113", "Lacrosse");
        this.sportTypes.set("114", "Beach Volleyball");
        this.sportTypes.set("115", "Virtual Running");
        this.sportTypes.set("116", "Virtual Cycling");
    }


    getDurationInHours() {
        return this.duration / MILLISECOUNDS_TO_HOUR
    }

    getDistance() {
        const trackData = this.features.find(feature => feature.type == FeatureType.track_metrics)

        return trackData ? (<AttributeTrack>trackData.attributes).distance : 0
    }

    getSportTypeText() {
        const text = this.sportTypes.get(this.sport_type_id)
        return text ? text : ""
    }
}

export enum FeatureType {
    weather = "weather",
    groups = "groups",
    origin = "origin",
    initial_values = "initial_values",
    heart_rate = "heart_rate",
    fastest_segments = "fastest_segments",
    track_metrics = "track_metrics",
    map = "map"
}

export interface EventFeature {
    type: FeatureType
    attributes: AttributeWeather | AttributeMap | AttributeTrack | AttributeFastestSegment | AttributeHeartRate | AttributeInitialValue | AttributeOrigin | AttributeGroups
}

export interface AttributeWeather {
    conditions: string
    temperature: number
    wind_speed: number
    wind_direction: number
    humidity: number
}

export interface AttributeMap {
    encoded_trace: string
    trace_available: boolean
    start_latitude: number
    start_longitude: number
}

export interface AttributeTrack {
    distance: number
    average_speed: number
    average_pace: number
    max_speed: string
    elevation_gain: number
    elevation_loss: number
    surface: string
}

export interface AttributeFastestSegment {
    segments: {
        distance: string
        duration: number
        started_at: number
    }[]
}

export interface AttributeHeartRate {
    average: number
    maximum: number
    trace_available: boolean
}

export interface AttributeInitialValue {
    distance: number
    duration: number
    pause: number
    trace_available: boolean
    sport_type: {
        id: number,
        type: string
    }
    start_time: number
    end_time: number
}

export interface AttributeOrigin {
    device: {
        name: string
        vendor: string
        os_version: string
    }
}

export interface AttributeGroups {
    groups: {
        id: string
        type: string
    }[]
}
